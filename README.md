# MRE Loading a custom conditions database

MRE demonstrating a problem when trying to load a custom conditions database
file for testing custom values. In this case, the "custom" conditions are a copy
of the `/PIXEL/PixelModuleFeMask` folder in `COOLOFL_PIXEL/OFLP200` created
using [AtlCoolCopy](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/AtlCoolCopy).

The custom file (called `mycool.db`) is inserted into Reco_tf using the
the following `--postExec` (v1):

```python
conddb.blockFolder("/PIXEL/PixelModuleFeMask")
conddb.addFolder("LOCAL","/PIXEL/PixelModuleFeMask", force=True)
conddb.addOverride("/PIXEL/PixelModuleFeMask","PixelModuleFeMask-empty")
```

or (v2):

```python
conddb.blockFolder("/PIXEL/PixelModuleFeMask")
conddb.addFolderWithTag("LOCAL","/PIXEL/PixelModuleFeMask", "PixelModuleFeMask-empty", force=True)
conddb.addOverride("/PIXEL/PixelModuleFeMask","PixelModuleFeMask-empty")`
```

This is inspired by [InDetAlignableOverride.py](https://gitlab.cern.ch/atlas/athena/-/blob/22.0/InnerDetector/InDetExample/InDetAlignExample/share/InDetAlignableOverride.py#L44) and [CoolIOVDbSvcConfigurable twiki](https://twiki.cern.ch/twiki/bin/view/AtlasComputing/CoolIOVDbSvcConfigurable#Blocking_a_folder_from_being_inc).

Both result in the following error:

```
00:02:36 ReadCondHandle      ERROR can't retrieve  ( 'CondAttrListCollection' , 'ConditionStore+/PIXEL/PixelModuleFeMask' )  via base class
```

## Solution
One also needs to specify the `className` for the conditions database item to be
loaded via the `CondInputLoader` class.

```python
conddb.blockFolder("/PIXEL/PixelModuleFeMask")
conddb.addFolder("LOCAL","/PIXEL/PixelModuleFeMask", className="CondAttrListCollection", force=True)
conddb.addOverride("/PIXEL/PixelModuleFeMask","PixelModuleFeMask-empty")
```
