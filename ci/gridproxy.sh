#!/bin/bash

# Check for needed environmental variables
if [ -z ${GRID_CERT+x} ]; then
    echo "Set GRID_CERT with base64 encoded usercert.pam"
    exit 1
fi

if [ -z ${GRID_KEY+x} ]; then
    echo "Set GRID_KEY with base64 encoded usercert.pam"
    exit 1
fi

if [ -z ${GRID_PASS+x} ]; then
    echo "Set GRID_PASS with base64 encoded usercert.pam"
    exit 1
fi

# Load ATLAS environment
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
export ALRB_localConfigDir=$HOME/localConfig
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh --quiet

# Setup certificate and get grid proxy
install -D -m 400 <(echo ${GRID_CERT} | base64 -d) ${HOME}/.globus/usercert.pem
install -D -m 600 <(echo ${GRID_KEY} | base64 -d) ${HOME}/.globus/userkey.pem
lsetup emi
echo ${GRID_PASS} | tr -d '=' | voms-proxy-init -voms atlas -pwstdin
